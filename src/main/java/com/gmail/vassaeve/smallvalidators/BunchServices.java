package com.gmail.vassaeve.smallvalidators;

/**
 * todo пучок сервисов.
 * Заготовка для ситуации, когда поле аннотировано Validator со множеством сервисов проверки
 */
public enum BunchServices {
    ;

    public static Boolean isEven(long value) {
        return value % 2 == 0;
    }

    public static boolean containsSpace(String str) {
        return str != null && str.contains(" ");
    }
}
