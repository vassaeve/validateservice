package com.gmail.vassaeve.smallvalidators;


/**
 * простой сервис проверки строки на пустоту
 */
public enum StringIsEmpty {
    ;

    public static Boolean apply(String s) {
        return s == null || s.isEmpty();
    }
}
