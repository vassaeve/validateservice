package com.gmail.vassaeve.smallvalidators;


/**
 * простой сервис проверки строки, что она не пустая
 */
public enum StringIsNotEmpty {
    ;

    public static Boolean apply(String s) {
        return s != null && !s.isEmpty();
    }
}
