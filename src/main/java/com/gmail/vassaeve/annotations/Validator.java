package com.gmail.vassaeve.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * декларация того, что поле класса валидируется. Содержит массив сервисов, которые последовательно валидируют объект
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Validator {
    Service[] value();
}
