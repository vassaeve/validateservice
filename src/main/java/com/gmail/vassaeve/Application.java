package com.gmail.vassaeve;


import com.gmail.vassaeve.dto.Dto1;
import com.gmail.vassaeve.dto.Pair;
import com.gmail.vassaeve.smallvalidators.StringIsEmpty;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public enum Application {
    ;

    public static void main(String... args) {
        Dto1 dto1 = Dto1.builder()
                .field1("field1")
                .field2(true)
                .field3(123456789)
                .pair(Pair.builder()
                        .key("key")
                        .value("value")
                        .build())
                .build();

        //валидация ручками
        Map<String, List<ValidateService.ValidateFieldFunction>> fieldValidators = new HashMap<>();
        fieldValidators.put(
                "field1",
                Arrays.asList(
                        ValidateService.ValidateFieldFunction.<String>builder()
                                .getter(dto1::getField1)
                                .fieldValidator(StringIsEmpty::apply)
                                .build()
                )
        );

        Map<String, List<Boolean>> result = ValidateService.validate(fieldValidators);
        result.forEach((key, value) -> {
            List<String> collect = value.stream()
                    .map(aBoolean -> aBoolean.toString())
                    .collect(Collectors.toList());
            log.info("{} - {}", key, String.join(", ", collect));
        });

    }

}
