package com.gmail.vassaeve.dto;

import com.gmail.vassaeve.annotations.BusinessObject;
import com.gmail.vassaeve.annotations.Service;
import com.gmail.vassaeve.annotations.Validator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@BusinessObject
public class Dto1 {
    @Validator({
            @Service(clazz = "", method = ""),
            @Service(clazz = "", method = ""),
    })
    private String field1;

    @Validator({
            @Service(clazz = "", method = ""),
            @Service(clazz = "", method = ""),
    })
    private Boolean field2;

    @Validator({
            @Service(clazz = "", method = "")
    }
    )
    private Integer field3;


    private Pair pair;

}
