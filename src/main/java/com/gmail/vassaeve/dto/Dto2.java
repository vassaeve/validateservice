package com.gmail.vassaeve.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Dto2 {

    private Integer field1;
    private String field2;
    private Boolean field3;
    private Point point;
}
