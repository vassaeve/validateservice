package com.gmail.vassaeve;

import com.gmail.vassaeve.annotations.BusinessObject;
import com.gmail.vassaeve.annotations.Service;
import com.gmail.vassaeve.annotations.Validator;
import com.gmail.vassaeve.smallvalidators.StringIsEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.lang3.reflect.MethodUtils;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public enum ValidateService {
    VALIDATE_SERVICE;

    private Map<Class<?>, Map<String, List<ValidateService.ValidateFieldFunction>>> clazzValidators;

    //todo еще будем хранить какие-нибудь настройки, карты валидаторов, валидируемые классы, поля и проч.
    ValidateService() {
        clazzValidators = new HashMap<>();
        try {
            List<Class> dtos = listAllBOClasses("com.gmail.vassaeve.dto");
            for (Class clazz : dtos) {

//                PropertyDescriptor[] propertyDescriptors = Introspector.getBeanInfo(clazz).getPropertyDescriptors();
//                Method readMethod = propertyDescriptors[0].getReadMethod();

                List<Field> allFieldsList = FieldUtils.getAllFieldsList(clazz);
                Map<String, List<ValidateService.ValidateFieldFunction>> fieldValidators = new HashMap<>();
                for (Field field : allFieldsList) {
                    Validator validator = field.getAnnotation(Validator.class);
                    if (validator != null) {
                        Service[] services = validator.value();

                        List<ValidateService.ValidateFieldFunction> list = new ArrayList<>();
                        for (Service service : services) {
//                            list.add(ValidateService.ValidateFieldFunction.<String>builder()
//                                    .getter(readMethod.invoke())
//                                    .fieldValidator(StringIsEmpty::apply) //какой-то сервис
//                                    .build());
                        }
                        fieldValidators.put(field.getName(), list);
                    }
                }
                clazzValidators.put(clazz, fieldValidators);
            }
        } catch (Exception e) {

        }
    }

    /**
     * примитивная функция. подготовка для обобщенной функции, принимающей POJO с декларацией валидаторов на каждом поле класса
     *
     * @param fieldValidators
     * @return
     */
    public static Map<String, List<Boolean>> validate(Map<String, List<ValidateFieldFunction>> fieldValidators) {
        Map<String, List<Boolean>> map = new HashMap<>();
        for (Map.Entry<String, List<ValidateFieldFunction>> entry : fieldValidators.entrySet()) {
            List<ValidateFieldFunction> validators = entry.getValue();
            List<Boolean> fieldResult = new ArrayList<>();
            for (ValidateFieldFunction validator : validators) {
                fieldResult.add(validator.validate());
            }
            map.put(entry.getKey(), fieldResult);
        }
        return map;
    }

    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ValidateFieldFunction<T> {
        private Supplier<T> getter;
        private Function<T, Boolean> fieldValidator;

        public boolean validate() {
            return fieldValidator.apply(getter.get());
        }
    }


    public static TreeSet<String> findClasses(String path, String packageName) throws MalformedURLException, IOException {
        TreeSet<String> classes = new TreeSet<>();
        if (path.startsWith("file:") && path.contains("!")) {
            String[] split = path.split("!");
            URL jar = new URL(split[0]);
            ZipInputStream zip = new ZipInputStream(jar.openStream());
            ZipEntry entry;
            while ((entry = zip.getNextEntry()) != null) {
                if (entry.getName().endsWith(".class")) {
                    String className = entry.getName().replaceAll("[$].*", "").replaceAll("[.]class", "").replace('/', '.');
                    if (className.startsWith(packageName)) {
                        classes.add(className);
                    }
                }
            }
        }
        File dir = new File(path);
        if (!dir.exists()) {
            return classes;
        }
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file.getAbsolutePath(), packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                String className = packageName + '.' + file.getName().replaceAll("[$].*", "").replaceAll("[.]class", "");//.substring(0, file.getName().length() - 6);
                classes.add(className);
            }
        }
        return classes;
    }

    public static List<Class> listAllBOClasses(String packageName) throws IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        String path = packageName.replace('.', '/');
        Enumeration resources = classLoader.getResources(path);
        List<String> dirs = new ArrayList<>();
        while (resources.hasMoreElements()) {
            Object oo = resources.nextElement();
            URL resource = (URL) oo;
            dirs.add(URLDecoder.decode(resource.getFile(), "UTF-8"));
        }
        TreeSet<String> classes = new TreeSet<>();
        for (String directory : dirs) {
            classes.addAll(findClasses(directory, packageName));
        }
        List<Class> classList = new ArrayList<>();

        classes.forEach(clazz -> {
            try {
                Class cls = Class.forName(clazz);
                BusinessObject annotation = (BusinessObject) cls.getAnnotation(BusinessObject.class);
                if (annotation != null) {
                    classList.add(cls);
                }
            } catch (ClassNotFoundException ex) {
            }
        });
        return classList;
    }

}
